var BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    watch: true,
    plugins: [
    new BrowserSyncPlugin({
            host: 'localhost',
            port: 8000,
            files: ['./dist/*.html', './dist/*.css'],
            server: {
                baseDir: ['dist']
            }
        })
    ]
}
