$(document).ready(function(){
	$("#content-wrapper").owlCarousel({
		items:1,
		autoplay:true,
		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>']
	});
});

$(document).ready(function(){
	$("#card-wrapper").owlCarousel({
		items:1,
		autoplay:true,
		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>']
	});
});
$(document).ready(function(){
	$("#section-wrapper").owlCarousel({
		items:1,
		autoplay:true,
		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>']
	});
});
$(document).ready(function(){
	$("#full-card-wrapper").owlCarousel({
		items:3,
		autoplay:true,
//		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>']
	});
});
$(document).ready(function(){
	$("#full-card-wrapper2").owlCarousel({
		items:3,
		autoplay:true,
//		margin:20,
		loop:true,
		nav:true,
		smartspeed:700,
		autoplayHoverPause:true,
		dots:false,
		navText:['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>']
	});
});
